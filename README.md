##### Description
> Source code in node.js for bot

##### Requirements
> Node JS (version 8.0.0, or above). https://nodejs.org/en/download/releases/

> npm. https://docs.npmjs.com/cli/install

> discord.js. npm install --save discord.js