// includes
const moment = require('moment');
const fs     = require('fs'); 

// funcs to export
module.exports = {
  // start
  clientReady: function () {
    console.log(`Logged in as ${client.user.tag}`);
  },

  // message handling
  clientMessage: function (message) {
    state.msg_count++;
    if (message.content.substring(0, 2) === '|:') {
      state.cmd_count++;
      const message_args = message.content.substring(2).split(" ");
      const message_cmd = message_args[0];

      switch (message_cmd) {
        case "foste":
          message.channel.send(`COMBADO`);
          break;
        case "joke":
          message.channel.send(`A tua mãe`);
          break;
        case "bonding":
          if (message_args.length != 3) {
            message.channel.send(`ERROR: Wrong 'bonding' input.\n|:bonding <operation> <number>`);
            break;
          }
          if (bondingUpdate(message, message_args[1], message_args[2]) != 0) console.log(`ERROR: In 'bondingUpdate()`);
          break;
        case "bonding_list":
          if (bondingList(message) != 0) {
            console.log(`ERROR: In 'bondingList()`);
          }
          break;
        case "msg_count":
          message.channel.send(`Total messages: ${state.msg_count}`);
          break;
        case "cmd_count":
          message.channel.send(`Total commands: ${state.cmd_count}`);
          break;
        case "test_img":
          message.channel.send(`Test image`, {files: ["file:///home"]});
          break;
        case "help":
          message.channel.send(`Command prefix is:\n  |:\n\nPossible commands are:\n  foste\n  joke\n  bonding <operation> <value>\n  msg_count\n  cmd_count`);
          break;
        default:
          message.channel.send(`ERROR: Unkown command: ${message_cmd}. 'help' for command help.`);
          break;
      }
    }
  }
};

// local functions
function bondingUpdate(message, op, value) {
  // get values from file
  fs.readFile(bonding_file, "utf-8", (err, data) => {
    if (err) console.log(err);

    const data_final = [];
    data_args = data.split(";");

    // for each user
    for (let i = 0; i < data_args.length - 1; i++) {
      user_author = data_args[i].split("=")[0];
      user_value = data_args[i].split("=")[1];

      // if author is in file
      if (user_author === message.author.username) {
        // choose op
        switch (op) {
          case "=":
            user_value = (Number(value)).toString();
            break;
          case "+":
            user_value = (Number(user_value) + Number(value)).toString();
            break;
          case "-":
            user_value = (Number(user_value) - Number(value)).toString();
            break;
          case "*":
            user_value = (Number(user_value) * Number(value)).toString();
            break;
          case "/":
            user_value = (Number(user_value) / Number(value)).toString();
            break;
          default:
            message.channel.send(`Unkown operation: ${op}. Supported operations are '=', '+', '-', '*', '/'.`);
            break;
        }
      }

      // put values in final array
      data_final.push(user_author);
      data_final.push('=');
      data_final.push(user_value);
      data_final.push(';');
    }

    // write to file
    fs.writeFile(bonding_file, data_final.join(""), function (err) {
      if (err) {
        console.log(err);
        return 1;
      }
      if (bondingList(message) != 0) console.log(`ERROR: In 'bondingList()`);
    });
  });

  return 0;
}

function bondingList(message) {
  // get values from file
  fs.readFile(bonding_file, "utf-8", (err, data) => {
    if (err) console.log(err);

    const data_final = [];
    let bonding_total = 0;
    data_args = data.split(";");

    // for each user
    for (let i = 0; i < data_args.length - 1; i++) {
      user_author = data_args[i].split("=")[0];
      user_value = data_args[i].split("=")[1];

      // change names
      if (user_author === "VolTroX") user_author = "Gonçalo";
      else if (user_author === "CyberVitor") user_author = "Vitor";
      else if (user_author === "Cock-gobblin\' cum-swallerin\' man") user_author = "Jaime";

      // sum values
      bonding_total += Number(user_value);

      // put values in final array
      data_final.push(user_author);
      data_final.push(" = ");
      data_final.push(user_value);
      data_final.push("\n");
    }
    // put total in final array
    data_final.push("\n**EQUIPA = ");
    data_final.push(bonding_total);
    data_final.push("**");

    // send message
    message.channel.send(data_final.join(""));
  });

  return 0;
}
